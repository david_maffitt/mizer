package org.nrg.dicom.mizer.tags;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestTagPath {

    @Test
    public void testCompareTo() {
        TagPath tagPath = new TagPath();
        tagPath.addTag( new TagPublic( 0x00100010));

        TagPath testPath = new TagPath();
        testPath.addTag( new TagPublic( 0x00100010));
        assertTrue( tagPath.compareTo( testPath) == 0);

        testPath = new TagPath();
        testPath.addTag( new TagPublic( 0x00080010));
        assertTrue( tagPath.compareTo( testPath) > 0);

        testPath = new TagPath();
        testPath.addTag( new TagPublic( 0x00180010));
        assertTrue( tagPath.compareTo( testPath) < 0);

        testPath = new TagPath();
        testPath.addTag( new TagPrivate( 0x00130010, "CTP"));
        assertTrue( tagPath.compareTo( testPath) < 0);

        testPath = new TagPath();
        testPath.addTag( new TagSequence( new TagPublic(0x00100010), null));
        testPath.addTag( new TagPublic(0x00080010));
        assertTrue( tagPath.compareTo( testPath) < 0);

        testPath = new TagPath();
        testPath.addTag( new TagPublic(0x000F0010));
        assertTrue( tagPath.compareTo( testPath) > 0);

        testPath = new TagPath();
        testPath.addTag( new TagPublic(0x001A0010));
        assertTrue( tagPath.compareTo( testPath) < 0);
    }

    @Test
    public void testSequenceCompareTo() {
        // (0010,0010) [1] / (0008,0010)
        TagPath tagPath = new TagPath();
        tagPath.addTag( new TagSequence( new TagPublic(0x00100010), 1));
        tagPath.addTag( new TagPublic(0x00080010));

        // (0010,0010) [1] / (0008,0010)
        TagPath testPath = new TagPath();
        testPath.addTag( new TagSequence( new TagPublic(0x00100010), 1));
        testPath.addTag( new TagPublic(0x00080010));
        assertTrue( tagPath.compareTo( testPath) == 0);

        // (0010,0010) [0] / (0008,0010)
        testPath = new TagPath();
        testPath.addTag( new TagSequence( new TagPublic(0x00100010), 0));
        testPath.addTag( new TagPublic(0x00080010));
        assertTrue( tagPath.compareTo( testPath) > 0);

        // (0010,0010) [2] / (0008,0010)
        testPath = new TagPath();
        testPath.addTag( new TagSequence( new TagPublic(0x00100010), 2));
        testPath.addTag( new TagPublic(0x00080010));
        assertTrue( tagPath.compareTo( testPath) < 0);

        // (0010,0010) / (0008,0010)
        testPath = new TagPath();
        testPath.addTag( new TagSequence( new TagPublic(0x00100010), 2));
        testPath.addTag( new TagPublic(0x00080010));
        assertTrue( tagPath.compareTo( testPath) < 0);
    }

    @Test
    public void testSequenceWildcard() {
        // (0010,0010) / (0008,0010)
        TagPath tagPath = new TagPath();
        tagPath.addTag( new TagSequence( new TagPublic(0x00100010), null));
        tagPath.addTag( new TagPublic(0x00080010));

        // (0010,0010) [1] / (0008,0010)
        TagPath testPath = new TagPath();
        testPath.addTag( new TagSequence( new TagPublic(0x00100010), 1));
        testPath.addTag( new TagPublic(0x00080010));
        assertTrue( tagPath.compareTo( testPath) > 0);

        // (0010,0010) [0] / (0008,0010)
        testPath = new TagPath();
        testPath.addTag( new TagSequence( new TagPublic(0x00100010), 0));
        testPath.addTag( new TagPublic(0x00080010));
        assertTrue( tagPath.compareTo( testPath) > 0);

        // (0010,0010) / (0008,0010)
        testPath = new TagPath();
        testPath.addTag( new TagSequence( new TagPublic(0x00100010), null));
        testPath.addTag( new TagPublic(0x00080010));
        assertTrue( tagPath.compareTo( testPath) == 0);
    }

    @Test
    public void testEvenDigitWildcard() {
        // (0010,00@0)
        TagPath tagPath = new TagPath();
        tagPath.addTag( new TagPublic("0010", "00@0"));

        // (0010,0010)
        TagPath testPath = new TagPath();
        testPath.addTag( new TagPublic(0x00100010));
        assertTrue( tagPath.compareTo( testPath) > 0);

        // (0010,00F0)
        testPath = new TagPath();
        testPath.addTag( new TagPublic(0x001000F0));
        assertTrue( tagPath.compareTo( testPath) < 0);

        // (0010,00E0)
        testPath = new TagPath();
        testPath.addTag( new TagPublic(0x001000E0));
        assertTrue( tagPath.compareTo( testPath) == 0);
    }

    @Test
    public void testOddDigitWildcard() {
        // (0010,00#0)
        TagPath tagPath = new TagPath();
        tagPath.addTag( new TagPublic("0010", "00#0"));

        // (0010,0010)
        TagPath testPath = new TagPath();
        testPath.addTag( new TagPublic(0x00100010));
        assertTrue( tagPath.compareTo( testPath) > 0);

        // (0010,00F0)
        testPath = new TagPath();
        testPath.addTag( new TagPublic(0x001000F0));
        assertTrue( tagPath.compareTo( testPath) == 0);

        // (0010,00E0)
        testPath = new TagPath();
        testPath.addTag( new TagPublic(0x001000F1));
        assertTrue( tagPath.compareTo( testPath) < 0);
    }

    @Test
    public void testHexDigitWildcard() {
        // (0010,00x0)
        TagPath tagPath = new TagPath();
        tagPath.addTag( new TagPublic("0010", "00x0"));

        // (0010,0010)
        TagPath testPath = new TagPath();
        testPath.addTag( new TagPublic(0x00100010));
        assertTrue( tagPath.compareTo( testPath) > 0);

        // (0010,00F0)
        testPath = new TagPath();
        testPath.addTag( new TagPublic(0x001000F0));
        assertTrue( tagPath.compareTo( testPath) == 0);

        // (0010,00E0)
        testPath = new TagPath();
        testPath.addTag( new TagPublic(0x001000F1));
        assertTrue( tagPath.compareTo( testPath) < 0);
    }
}
