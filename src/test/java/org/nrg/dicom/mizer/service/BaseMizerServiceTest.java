package org.nrg.dicom.mizer.service;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;
import org.nrg.test.workers.resources.ResourceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestMizerConfig.class)
public class BaseMizerServiceTest {
    @Test
    // Ignoring these tests here. They are more integration tests requiring implementatations of Mizers that are external
    // and dependent on the Mizer lib.
    @Ignore
    public void testService() throws MizerException {
//        try {
//            List<Mizer> mizers = new ArrayList<>();
//            mizers.add( new org.nrg.dicom.dicomedit.mizer.DE6Mizer());
//            MizerService mizerService = new org.nrg.dicom.mizer.service.impl.BaseMizerService( mizers);
//
//            MizerContextWithScript context = new MizerContextWithScript();
//
//            final File testFile = File.createTempFile("mizer.", ".dcm");
//            Files.copy( DICOM_TEST.toPath(), testFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
//            final DicomObjectI dicom = DicomObjectFactory.newInstance( testFile);
//            final DicomObject dcm4che2Object = dicom.getDcm4che2Object();
//
//            assertEquals("head^DHead", dcm4che2Object.getString(Tag.StudyDescription));
//            assertEquals("Sample Patient", dcm4che2Object.getString(Tag.PatientName));
//            assertEquals("Sample ID", dcm4che2Object.getString(Tag.PatientID));
//            assertEquals("SIEMENS", dcm4che2Object.getString(Tag.Manufacturer));
//            assertEquals("Hospital", dcm4che2Object.getString(Tag.InstitutionName));
//            assertTrue( dcm4che2Object.contains(Tag.PixelData));
//
//        }
//        catch( IOException e) {
//            fail("Unexpected exception: " + e);
//        }
    }

    private static final ResourceManager _resourceManager = ResourceManager.getInstance();
    private static final File DICOM_TEST       = _resourceManager.getTestResourceFile("dicom/1.MR.head_DHead.4.1.20061214.091206.156000.1632817982.dcm");

    @Autowired
    private MizerService _service;
}
