/*
 * DicomEdit: DicomTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.objects;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.io.DicomInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 * Created by drm on 5/16/16.
 */
public class DicomTest {

    private org.dcm4che2.data.DicomObject dobj;

    public DicomTest( File f) throws IOException {
        InputStream fin = new FileInputStream( f);
        if (f.getName().endsWith("gz")) {
            fin = new GZIPInputStream(fin);
        }
        DicomInputStream dis = new DicomInputStream(fin);
        dobj = dis.readDicomObject();
    }

    public List<String> getTagPaths(String parentPath, org.dcm4che2.data.DicomObject dob) {
        List<String> tagPaths = new ArrayList<>();
        Iterator<DicomElement> it =  dob.iterator();
        while( it.hasNext()) {
            StringBuilder sb;
            sb = ( parentPath != null)? new StringBuilder( parentPath): new StringBuilder();

            DicomElement de = it.next();
            sb.append( getTagPath( de));
            if( de.hasItems()) {
                for( int i = 0; i < de.countItems(); i++) {
                    tagPaths.addAll( getTagPaths( sb.toString(), de.getDicomObject(i)));
                }
            }
            else {
                tagPaths.add( sb.toString());
            }
        }
        return null;
    }

    private String getTagPath( DicomElement de ) {
        return de.toString();
    }

    public static void main( String[] args) {
        File f = new File("/home/drm/projects/nrg/dicomeditv4/src/main/resources/data/242.dcm");
        try {
            DicomTest dt = new DicomTest(f);
        }
        catch( Exception e) {
            e.printStackTrace();
        }
    }
}
