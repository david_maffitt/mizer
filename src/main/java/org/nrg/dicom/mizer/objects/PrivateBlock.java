/*
 * DicomEdit: PrivateBlock
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.objects;

import org.nrg.dicom.mizer.tags.Tag;

/**
 * Encapsulate the group number, block tag, and creator ID string that constitute a private block.
 *
 * Add the block tag o support vendors that put multiple blocks in the same group with the same creator id. For example
 * Fuji has data like
 * (0029,0012) = "FujiFILM TM"
 * (0029,00E1) = "FujiFILM TM"
 * (0029,1231) = 'some data'
 * (0029,E131) = 'some data'
 *
 * We add the block tag to address these blocks uniquely.  The example defines two private blocks {0x0029, 0x12, "FujiFILM TM"} and
 * {0x0029, 0xE1, "FujiFILM TM"}
 *
 * A block tag value of 0 is used to denote an unknown/unassigned block.
 *
 *
 */
public class PrivateBlock {
    private final short group;
    private final short blockTag;
    private final String creatorID;

    public PrivateBlock( int tag, String creatorID) {
        this.group = (short) (tag >>> 16);
        this.blockTag = getBlockTag( tag);
        this.creatorID = creatorID;
    }

    public PrivateBlock( short group, String creatorID) {
        this.group = group;
        this.blockTag = 0;
        this.creatorID = creatorID;
    }

    public short getGroup() {
        return group;
    }

    public short getBlockTag() { return blockTag; }

    public String getCreatorID() {
        return creatorID;
    }

    private short getBlockTag( int tag) {
        short blockTag = (short) ((tag & 0xFF00)>>>8);
        if( blockTag == 0) {
            // tag is the creator id.
            blockTag = (short) (tag & 0x00FF);
        }
        return blockTag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrivateBlock that = (PrivateBlock) o;

        if (getGroup() != that.getGroup()) return false;
        if (getBlockTag() != that.getBlockTag()) return false;
        return getCreatorID() != null ? getCreatorID().equals(that.getCreatorID()) : that.getCreatorID() == null;

    }

    @Override
    public int hashCode() {
        int result = getGroup() + getBlockTag();
        result = 31 * result + (getCreatorID() != null ? getCreatorID().hashCode() : 0);
        return result;
    }

    public static void main( String[] args) {
        PrivateBlock pb = new PrivateBlock( 0x0017E110, "XYZ") ;

        System.out.println("Group = " + Tag.padLeft(Integer.toHexString(pb.getGroup()), 4, '0'));
        System.out.println("BlockTag = " + Integer.toHexString(pb.getBlockTag()));
    }
}
