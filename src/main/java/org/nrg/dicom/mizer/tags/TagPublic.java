/*
 * DicomEdit: TagPublic
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.tags;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implements a container for public tags.
 */
public class TagPublic extends Tag {

    private final String group;
    private final String element;
    private final boolean singular;
    private String regex;

    private static final Logger logger = LoggerFactory.getLogger( TagPublic.class);

    public TagPublic(String group, String element) {
        this.group = group;
        this.element = element;
        singular = ! (hasWildCard( group) || hasWildCard( element));
    }

    public TagPublic( int tag) {
        String s = padLeft( Integer.toHexString( tag), 8, '0');
        this.group = s.substring(0,4);
        this.element = s.substring(4,8);
        this.singular = true;
    }

    public String getGroup() {
        return group;
    }

    public String getElement() {
        return element;
    }

    @Override
    public String asString() {
        return group + element;
    }

    public String toString() {
        return asString();
    }

    @Override
    public boolean isSingular() {
        return singular;
    }

    @Override
    public String getRegex() {
        regex = (regex != null)? regex: computeBlockRegex( group) + computeBlockRegex( element);
        return regex;
    }
}
