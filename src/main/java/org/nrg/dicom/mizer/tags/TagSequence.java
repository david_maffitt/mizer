/*
 * DicomEdit: TagSequence
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.tags;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Model Tags of type sequence (VR == SQ).
 *
 *
 */
public class TagSequence extends Tag {

    private final Tag tag;
    private final String itemNumber;
    private final boolean singular;
    private String regex;

    private static final Logger logger = LoggerFactory.getLogger( TagPublic.class);

    public TagSequence( Tag tag, String itemNumber) {
        this.tag = tag;
        this.itemNumber = (itemNumber != null)? itemNumber: String.valueOf(Tag.ITEMNUMBER_WILDCARD_CHAR);
        this.singular = ( ! hasWildCard(itemNumber)) && tag.isSingular();
    }

    public TagSequence( Tag tag, int itemNumber) {
        this.tag = tag;
        this.singular = true;
        this.itemNumber = Integer.toString(itemNumber);
    }

    public Tag getTag() {
        return tag;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    /**
     * Throws NumberFormat exception if the item number is a wild card and does not map to a unique integer.
     *
     * @return Returns the item number as an integer.
     */
    public int getItemNumberAsInt() {
        return Integer.parseInt( itemNumber);
    }

    @Override
    public String getGroup() {
        return tag.getGroup();
    }

    @Override
    public String getElement() {
        return tag.getElement();
    }

    @Override
    public String asString() {
        String item = "";
        if( itemNumber != null) item = "[" + itemNumber + "]";
        return tag.asString() + item;
    }

    public String toString() {
        return asString();
    }

    @Override
    public boolean isSingular() {
        return singular;
    }

    protected boolean isItemWild() {
        return String.valueOf(Tag.ITEMNUMBER_WILDCARD_CHAR).equals( itemNumber);
    }

    /**
     * integer less than (before) wild card.
     *
     * @param thatTag
     * @return
     */
    @Override
    public int compareTo( Tag thatTag) {
        int c = super.compareTo( thatTag);
        if( c == 0 && thatTag instanceof TagSequence) {
            TagSequence thatSeqTag = (TagSequence) thatTag;
            if( isItemWild()) {
                c = ( thatSeqTag.isItemWild())? 0: 1;
            }
            else {
                c = ( thatSeqTag.isItemWild())? -1: this.getItemNumberAsInt() - thatSeqTag.getItemNumberAsInt();
            }
        }
        return c;
    }

    @Override
    public String getRegex() {
        regex = (regex != null)? regex: computeRegex();
        return regex;
    }

    private String computeRegex() {
        String itemNumberRegex = "";
        String item = ( itemNumber != null)? itemNumber: "%";
        switch (item) {
            case "%":
                itemNumberRegex = "(\\[[0-9]+\\])?+";
                break;
            default:
                itemNumberRegex = "\\[" + item + "\\]";
                break;
        }
        return tag.getRegex() + itemNumberRegex;
    }
}
