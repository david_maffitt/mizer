<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ mizer: pom.xml
  ~ XNAT https://www.xnat.org
  ~ Copyright (c) 2020, Washington University School of Medicine
  ~ All Rights Reserved
  ~
  ~ Released under the Simplified BSD.
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.nrg</groupId>
        <artifactId>parent</artifactId>
        <version>1.8.3</version>
    </parent>

    <groupId>org.nrg.dicom</groupId>
    <artifactId>mizer</artifactId>
    <version>1.2.1</version>

    <name>NRG Mizer Framework</name>
    <description>Common Service API For anonymizing DICOM images</description>

    <scm>
        <url>https://bitbucket.org/xnatdev/mizer</url>
        <connection>scm:git:git://bitbucket.org/xnatdev/mizer.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:xnatdev/mizer.git</developerConnection>
    </scm>

    <issueManagement>
        <system>JIRA</system>
        <url>https://issues.xnat.org</url>
    </issueManagement>

    <licenses>
        <license>
            <name>Simplified BSD License</name>
            <url>https://www.opensource.org/licenses/BSD-2-Clause</url>
        </license>
    </licenses>

    <organization>
        <name>Neuroinformatics Research Group</name>
        <url>https://nrg.wustl.edu</url>
    </organization>

    <dependencies>
        <!--BEGIN  NRG dependencies -->
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>dicomtools</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>*</groupId>
                    <artifactId>*</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>framework</artifactId>
        </dependency>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>transaction</artifactId>
        </dependency>
        <!--END  NRG dependencies -->

        <dependency>
            <groupId>dcm4che</groupId>
            <artifactId>dcm4che-core</artifactId>
        </dependency>
        <dependency>
            <groupId>dcm4che</groupId>
            <artifactId>dcm4che-iod</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-orm</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-codec</groupId>
            <artifactId>commons-codec</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.uuid</groupId>
            <artifactId>java-uuid-generator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>jsr305</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- BEGIN Test dependencies -->
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.sf.ehcache</groupId>
            <artifactId>ehcache-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- END Test dependencies -->
    </dependencies>

    <build>
       <finalName>nrg-mizer-${project.version}</finalName>
        <plugins>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-source-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-javadoc-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>org.nrg.maven.artifacts.release</id>
            <name>XNAT Release Maven Repo</name>
            <url>https://nrgxnat.jfrog.io/nrgxnat/libs-release</url>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>fail</checksumPolicy>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>org.nrg.maven.artifacts.snapshot</id>
            <name>xnat snapshot maven repo</name>
            <url>https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
                <checksumPolicy>fail</checksumPolicy>
            </snapshots>
        </repository>
    </repositories>

 </project>
